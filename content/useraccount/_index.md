+++
title = "登录平台"
description = ""
weight = 1
+++

Alauda DevOps 支持多种登录认证方式，您可通过以下三种类型的账号登录平台：

- [使用管理员账号登录]({{< relref "useraccount/admin.md" >}})

- [使用 LDAP 认证账号登录]({{< relref "useraccount/ldap.md" >}})

- [使用 OIDC 认证账号登录]({{< relref "useraccount/oidc.md" >}})