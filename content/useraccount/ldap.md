+++
title = "使用 LDAP 认证账号登录"
description = "使用 LDAP 认证账号登录 DevOps 平台。"
weight = 2
+++

如果管理员将 LDAP 配置为用户登录认证方式，普通用户应使用 LDAP 认证账号登录 DevOps 平台。

**操作步骤**

1. 在浏览器中打开 Alauda DevOps 平台。 

2. 在登录界面选择 LDAP 服务的登录入口，例如 OpenLDAP 作为 LDAP 认证服务。

3. 输入 LDAP 认证账号和密码。

5. 单击 **登录**。