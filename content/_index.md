+++
title = "Alauda DevOps"
description = ""
+++

# Alauda DevOps

Alauda DevOps 基于平台管理员和项目中普通用户的两种不同视角，将功能区分并细化，并基于 Jenkins 完成从代码提交到应用部署的 DevOps CICD 全流程，实现高速交付应用和服务的能力，简化了传统软件开发和运维的复杂流程，可有效的支撑业务创新，助力企业快速将产品推向市场。

使用 Alauda DevOps，有助于：

* 简化日常工作，实现流程最大程度的自动化

* 快速迭代，提升组织工作效率

* 角色清晰，保证资源隔离，同时保证项目合作

## 平台管理

平台管理员专注于平台管理和项目内的资源、服务管理，可以创建项目、集成 Jenkins 服务、管理 Secret 等。

* [项目]({{< relref "admin/project/_index.md" >}})

* [服务]({{< relref "admin/service/_index.md" >}})

* [Secret]({{< relref "admin/secret/_index.md" >}})

* [部署 Jenkins]({{< relref "admin/deploy/_index.md" >}})


## 项目管理

项目成员专注于项目的具体业务，开发测试负责项目中具体业务的开发和测试，项目经理负责对项目的监控和管理。

* [应用]({{< relref "project/app/_index.md" >}})

* [流水线]({{< relref "project/pipeline/_index.md" >}})

* [Secret]({{< relref "project/secret/_index.md" >}})

* [成员]({{< relref "project/members/_index.md" >}})

