+++
title = "查看 Secret"
description = "查看已创建的 secret 详情，例如：基本信息、类型信息。"
weight = 2
+++

查看已创建的 secret 详情，例如：基本信息、类型信息。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，单击右上角的 ***账号名称*** > **平台管理** > **Secret**。

2. 在 Secret 页面，单击要查看详情的 ***Secret 名称***。

3. 在 ***Secret 名称*** 页面，查看 secret 的基本信息。例如：创建时间、所属项目等。

4. 查看 secret 的类型信息。例如：用户名和密码、镜像服务或 Opaque。

5. 单击 ![eye](/img/eye.png)，查看隐藏的密码。