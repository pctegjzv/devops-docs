+++
title = "Secret"
description = ""
weight = 3
+++

使用 secret 保存敏感信息，例如：密码、token、 ssh key 等，保存数据更安全灵活，使数据免于暴露到镜像或 Pod Spec 中。

Secret 支持三种类型：

* 用户名和密码：用于验证访问 Jenkins 服务或代码仓库用户的身份信息。

* 镜像服务：用于存储私有 docker registry 的认证信息。

* Opaque：用于存储密码、密钥等。Opaque 类型的数据是一个 map 类型的 base64 编码格式。


{{%children style="card" description="true" %}}