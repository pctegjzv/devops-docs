+++
title = "平台管理员"
description = ""
weight = 2
alwaysopen = true
+++

平台管理员专注于平台管理和项目内的资源、服务管理，可以创建项目、集成 Jenkins 服务、管理 Secret 等。

- [项目]({{< relref "admin/project/_index.md" >}})

- [服务]({{< relref "admin/service/_index.md" >}})

- [Secret]({{< relref "admin/secret/_index.md" >}})

- [部署 Jenkins]({{< relref "admin/deploy/_index.md" >}})
