+++
title = "查看项目详情"
description = "在项目的详情页面，查看详细信息、服务、资源、项目成员信息。"
weight = 2
+++

在项目的详情页面，查看详细信息、服务、资源、项目成员信息。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，单击右上角的 ***账号名称*** > **平台管理** > **项目**。

2. 在项目页面，单击要查看详情的 ***项目名称***。

3. 在 **详细信息** 栏，查看项目的基本信息。例如：项目名称、创建时间、显示名称、描述。

4. 在 **服务** 栏，查看 Jenkins 的服务信息。例如：已绑定的 Jenkins 服务，以及其他操作。

   * 单击 **绑定 Jenkins 服务**，绑定新的 Jenkins 服务。参考[绑定 Jenkins 服务]({{< relref "admin/service/jenkins/bindJenkinsservice.md" >}})。

   * 单击 ***Jenkins 服务名称***，进入 Jenkins 服务详情页面。

   * 单击 ***Secret 名称***，进入 Secret 详情页面。

5. 在 **资源** 栏，查看项目的 secret。

   * 单击 **添加 Secret**，在项目中添加 secret，用于保存项目的敏感信息。参考[添加 Secret]({{< relref "admin/project/addsecret.md" >}})。

   * 单击 ***Secret 名称***，进入 Secret 详情页面。

6. 在 **项目成员** 栏，查看项目中的成员信息，并添加和删除成员。参考[添加成员]({{< relref "admin/project/addmembers.md" >}})和[删除成员]({{< relref "admin/project/deletemember.md" >}})。