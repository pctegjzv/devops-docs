+++
title = "项目"
description = ""
weight = 1
+++

组织的成员通过项目连接在一起，进行业务的分配和合作，最终完成业务。

平台管理员通过创建项目，配置项目中的成员以及项目经理，对项目进行管理。

{{%children style="card" description="true" %}}