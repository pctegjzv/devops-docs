+++
title = "绑定 Jenkins 服务"
description = "绑定 Jenkins 服务，实现服务的持续集成和升级。绑定 Jenkins 服务前，需要集成 Jenkins 服务并创建 secret。"
weight = 3
+++

绑定 Jenkins 服务，实现服务的持续集成和升级。绑定 Jenkins 服务前，需要集成 Jenkins 服务并创建 secret。多个项目可以绑定同一个 Jenkins 服务，一个项目也可以绑定多个 Jenkins 服务。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，单击右上角的 ***账号名称*** > **平台管理** > **项目**。

2. 在项目页面，单击要绑定 Jenkins 服务的 ***项目名称***。

3. 在项目页面，单击 **服务** > **绑定 Jenkins 服务**。

4. 在 **绑定 Jenkins 服务** 窗口，填写以下信息：

   * 在 **Jenkins 服务** 框中，选择一个已集成的 Jenkins 服务。在同一个项目里已绑定的 Jenkins 服务，不支持再次绑定。参考[集成 Jenkins 服务]({{< relref "admin/service/jenkins/inteJenkins.md" >}})。

   * 在 **Secret** 框中，选择一个已创建的 secret。绑定 Jenkins 服务时，会自动过滤显示类型为用户名和密码的 secret 类型。参考[创建 Secret]({{< relref "admin/secret/createsecret.md" >}})。

   * (非必填) 在 **描述** 框中，输入绑定 Jenkins 服务的描述信息。

5. 单击 **创建**。