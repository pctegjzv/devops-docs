+++
title = "集成 Jenkins 服务"
description = "集成 Jenkins 服务，实现服务的持续集成和升级。"
weight = 1
+++

集成 Jenkins 服务，实现服务的持续集成和升级。集成 Jenkins 服务前，需要安装 Alauda DevOps Jenkins 插件。Alauda DevOps 支持 Jenkins v2.83。参考[部署 Jenkins]({{< relref "admin/deploy/_index.md" >}})。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，单击右上角的 ***账号名称*** > **平台管理** > **Jenkins 服务**。

2. 在 Jenkins 服务页面，单击 **集成 Jenkins 服务**。

3. 在集成 Jenkins 服务页面，填写以下信息：

   - 在 **服务名称** 框中，输入已部署的 Jenkins 的服务名称。在 Jenkins 服务上的位置：**系统管理** > **系统设置**。  
     名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 30 个。不支持以中横线开头或结尾。

   - (非必填) 在 **显示名称** 框中，输入 Jenkins 服务的显示名称，支持中文字符。如未输入，默认显示为空。

   - (非必填) 在 **描述** 框中，输入 Jenkins 服务的描述信息。

   - 在 **服务地址** 框中，输入 Jenkins 的服务地址。如果输入 IP，需要添加端口号。

4. 单击 **创建**。  
   在 Jenkins 服务的详情页面，查看服务名称、服务地址等。