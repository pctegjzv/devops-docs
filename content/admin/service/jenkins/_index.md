+++
title = "Jenkins"
description = ""
weight = 1
+++

Jenkins 是一个开源软件项目，是基于 Java 开发的一种持续集成工具，用于监控及执行持续重复的工作。

在 DevOps 上集成并绑定 Jenkins 服务，实现服务的持续集成和升级。

{{%children style="card" description="true" %}}