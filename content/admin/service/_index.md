+++
title = "服务"
description = ""
weight = 2
alwaysopen = true
+++

服务管理包含 Jenkins 服务、代码仓库服务等。

- [Jenkins]({{< relref "admin/service/jenkins/_index.md" >}})
