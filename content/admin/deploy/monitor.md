+++
title = "监控部署状态"
description = "监控部署的 Jenkins 服务状态。"
weight = 2
+++

监控部署的 Jenkins 服务状态。

**操作步骤**

1. 在命令行中输入命令，查看部署相关的信息：`kubectl get deploy --all-namespaces`。

2. 查看 pod 状态：`kubectl get pod -n default -w`。  
如果 pod 是 `Running` 状态，Jenkins 服务就是执行中的状态。

	![Jenkinsrunning](/img/Jenkinsrunning.png?classes=big)
3. 查看服务的状态：`kubectl get svc -n default`，查看部署所在的 ClusterIP 和对应的端口。

	![monitorservice](/img/monitorservice.jpg?classes=big)

4. 用具有平台管理员权限的成员登录 DevOps 平台，单击右上角的 ***账号名称*** > **项目管理**。

4. 在项目列表页面，单击已部署成功的 ***项目名称***。

3. 在 **应用** 栏，查看已创建成功的应用。

4. 单击 ***应用名称***，在应用的详情页面，在 **其他资源** 区域，查看已创建的类型为 **service** 的资源。
	