+++
title = "配置插件"
description = "为了同步 DevOps 平台的数据并在 Jenkins 中执行流水线，需要先在 DevOps 平台上集成并绑定 Jenkins 服务，再在 Jenkins 服务中进行相关配置。"
weight = 3
+++

为了同步 DevOps 平台的数据并在 Jenkins 中执行流水线，需要先在 DevOps 平台上集成并绑定 Jenkins 服务，再在 Jenkins 服务中进行相关配置。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，完成操作：[集成 Jenkins 服务]({{< relref "admin/service/jenkins/inteJenkins.md" >}})。

2. 完成操作：[绑定 Jenkins 服务]({{< relref "admin/service/jenkins/bindJenkinsservice.md" >}})。

3. 登录已部署的 Jenkins 服务。  
Jenkins 服务地址是：HTTP/HTTPS 协议 + IP + 端口号或 HTTP/HTTPS 协议 + 域名。

4. 单击 **系统管理** > **系统设置**。

5. 在 **Alauda Jenkins Sync** 区域，在 **Jenkins service** 框中，输入已部署的 Jenkins 的服务名称。此名称需要和 DevOps 平台上集成 Jenkins 的服务名称一致。  
参考[集成 Jenkins 服务]({{< relref "admin/service/jenkins/inteJenkins.md" >}})。

6. 单击 **保存**。  
设置后，在 DevOps 平台上已绑定的项目中创建的流水线，会自动同步到 Jenkins 服务上。
