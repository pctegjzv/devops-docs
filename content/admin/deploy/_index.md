+++
title = "部署 Jenkins"
description = "在 DevOps 平台，直接部署 Jenkins 服务，用于监控及执行持续重复的工作，实现服务的持续集成和升级。"
weight = 4
alwaysopen = true
+++

在 DevOps 平台，直接部署 Jenkins 服务，用于监控及执行持续重复的工作，实现服务的持续集成和升级。

{{%children style="card" description="true" %}}
