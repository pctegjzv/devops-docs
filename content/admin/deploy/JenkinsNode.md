+++
title = "动态分配资源"
description = "在 Kubernetes 集群中，根据任务属性，为 Jenkins 动态分配临时容器，并作为 node 节点加入 Jenkins 集群，实现资源的分配。"
weight = 4
+++

在 Kubernetes 集群中，根据任务属性，为 Jenkins 动态分配临时容器，并作为 node 节点加入 Jenkins 集群，实现资源的分配；当任务执行结束后，Jenkins 会自动删除相关节点，并销毁相关容器，实现资源的释放。


**操作步骤**

1. 创建一个基于 Alpine Linux 系统的 JNLP (Java Network Launching Protocol) 镜像，使用此镜像时会启动 Jenkins node，并访问 Jenkins master。例如：镜像 index.alauda.cn/alaudaorg/jenkins-node-all-alpine:e9326232a5f25aa323c610d8895d55de961fe6ed。

2. 登录已部署的 Jenkins 服务。  
Jenkins 服务地址是：HTTP/HTTPS 协议 + IP + 端口号或 HTTP/HTTPS 协议 + 域名。

4. 单击 **系统管理** > **系统设置**。

5. 在 **云** 区域，单击 **新增一个云** > **Kubernetes**，创建 Jenkins Kubernetes 云：
	1. 在 **Name** 框中，输入新增 Jenkins Kubernetes 云的名称，用于识别此云。
	
	2. 在 **Kubernetes Namespace** 框中，输入存储资源的命名空间名称。建议此名称和部署 Jenkins 的 YAML 中的参数一致。参考[部署 Jenkins]({{< relref "admin/deploy/deployJenkins.md" >}})。
	
	3. 在 **Jenkins URL** 框中，输入集群内访问 Jenkins master 的 HTTP 地址。
	
	4. 在 **Jenkins tunnel** 框中，输入集群内访问 Jenkins node 的不带协议的地址和端口。  
	可以使用服务的 IP 地址和端口来访问。查看服务信息使用的命令：`kubectl get svc -n <namespace>`。
	
	5. 根据具体的业务需要，填写其他参数。

6. 在 **Images** 区域，单击 **Add Pod Template** > **Kubernetes Pod Template**，使用 Pod Template 设置容器，为不同的 node 配置：
	1. 在 **Name** 框中，输入 pod 的名称。

	2. 在 **Labels** 框中，输入此 node 支持的 Jenkins agent 的标签。
	
	3. 在 **Containers** 区域，单击 **Add Container** > **Container Template**：
		1. 在 **Name** 框中，输入运行容器的名称 `jnlp`，用于运行 JNLP 代理服务。
		
		2. 在 **Docker image** 框中，输入 JNLP 代理服务的镜像：`index.alauda.cn/alaudaorg/jenkins-node-all-alpine:e9326232a5f25aa323c610d8895d55de961fe6ed`。
		
		3. 在 **Always pull image** 复选框，不选择此选项，即不用经常拉取镜像。
		
		4. 在 **Working directory** 框中，输入 node 的工作目录。例如：`/home/jenkins`。
		
		5. 在 **Command to run** 框中，不输入任何命令，即使用镜像中 entrypoint 的值。
		
		6. 在 **Arguments to pass to the command** 框中，输入传递给命令行的参数：`${computer.jnlpmac} ${computer.name}`。
		
		7. 在 **Allocate pseudo-TTY** 复选框，不选择此选项，即不通过强制 SSH 分配一个伪 TTY 设备与远程应用程序进行交互。
	
	4. 在 **Volumes** 区域，单击 **Add Volume** > **Host Path Volume**，使用本地存储：
		1. 在 **Host path** 框中，输入主机的 docker.sock 文件路径。例如：`/var/run/docker.sock`。 
		
		2. 在 **Mount path** 框中，输入 pod 中挂载的存储卷路径。例如：`/var/run/docker.sock`。
	7. 在 **Timeout in seconds for Jenkins connection** 框中，输入 Jenkins 等待 JNLP agent 成功建立连接的秒数。例如：`200` 秒。
	
	8. 单击 **保存**。
