+++
title = "部署 Jenkins"
description = "在 DevOps 平台，用 YAML 部署 Jenkins 服务，同步数据到 Jenkins 服务，用于监控及执行持续重复的工作，实现服务的持续集成和升级。"
weight = 1
+++

在 DevOps 平台，用 YAML 部署 Jenkins 服务，同步数据到 Jenkins 服务，用于监控及执行持续重复的工作，实现服务的持续集成和升级。

**操作步骤**

1. 用具有平台管理员权限的成员登录 DevOps 平台，单击 **Yaml 创建**。

2. 在 Yaml 创建页面，在 **YAML (读写)** 区域，输入部署 Jenkins 服务的 YAML 内容。可参考样例：   

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  annotations:
    alauda.io/product: "Alauda DevOps"
    aladua.io/product.version: "v0.3"
  labels:
    app: "<app>"
  name: "<deployment_name>"
  namespace: "<namespace>"
spec:
  replicas: 1
  selector:
    matchLabels:
      app: "<app>"
  strategy:
    rollingUpdate:
      maxSurge: 1
      maxUnavailable: 1
    type: RollingUpdate
  template:
    metadata:
      labels:
        app: "<app>"
    spec:
      containers:
      - env:
        - name: JENKINS_PASSWORD
          value: "<123456>"
        image: index.alauda.cn/alaudaorg/jenkins:master-v2.83
        imagePullPolicy: IfNotPresent
        name: jenkins
        ports:
        - containerPort: 8080
          name: http
          protocol: TCP
        - containerPort: 50000
          name: jnlp
          protocol: TCP
        resources:
          requests:
            cpu: "<1>"
            memory: <1Gi>
          limits:
            cpu: "<1.5>"
            memory: <1.5Gi>
        volumeMounts:
        - mountPath: /var/jenkins_home
          name: jenkins-home
        - mountPath: /var/run/docker.sock
          name: docker
      dnsPolicy: ClusterFirst
      initContainers:
      - image: index.alauda.cn/alaudak8s/jenkins-plugin-injector:release
        imagePullPolicy: IfNotPresent
        name: install-plugin
        volumeMounts:
        - mountPath: /var/jenkins_home
          name: jenkins-home
      - image: index.alauda.cn/alaudak8s/jenkins-client-plugin:release
        imagePullPolicy: IfNotPresent
        name: install-dsl-plugin
        volumeMounts:
        - mountPath: /var/jenkins_home
          name: jenkins-home
      nodeSelector:
        kubernetes.io/hostname: "<node>"
      restartPolicy: Always
      schedulerName: default-scheduler
      securityContext:
        runAsUser: 0
      serviceAccount: default
      terminationGracePeriodSeconds: 30
      volumes:
      - hostPath:
          path: "<file/path>"
          type: ""
        name: jenkins-home
      - hostPath:
          path: "<docker/path>"
          type: ""
        name: docker
---
apiVersion: v1
kind: Service
metadata:
  annotations:
    alauda.io/product: "Alauda DevOps"
    aladua.io/product.version: "v0.3"
  labels:
    app: "<app>"
  name: "<service_name>"
  namespace: "<namespace>"
spec:
  externalTrafficPolicy: Cluster
  ports:
  - name: http
    nodePort: <port>
    port: 8080
    protocol: TCP
    targetPort: 8080
  - name: jnlp
    nodePort: <port>
    port: 50000
    protocol: TCP
    targetPort: 50000
  selector:
    app: <app>
  sessionAffinity: None
  type: NodePort
```
根据具体的环境要求，需要修改的参数：  

  * 已存在的 `namespace` 字段。

  * `admin` 默认密码：当第一次部署 Jenkins 时，在 `containers` 的 `env` 字段下，当 key 是 `JENKINS_PASSWORD` 时，Jenkins 会自动设置 value 为 admin 密码。重新部署时，密码不会被修改。

  * `resources` 字段：在 `containers` 的 `resources` 字段，为了控制 Jenkins 使用的资源，部署时可以调整 `cpu` 和 `memory`。参考[Kubernetes 的官方文档](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/)。

  * `serviceAccount` 字段：Jenkins 的插件会调度 Kubernetes 的 API 并同步信息或创建静态的 node，所以此参数比较重要。需要保证部署的 namespace 中 service account 存在并有足够的权限。建议在部署 Jenkins 前，先创建 service account，然后设置对应的 cluster role binding。在命令行中可参考样例：  
    	1. 创建 service account，并使用 `-n` 参数来设置项目名称： `kubectl create serviceaccount <service_account_name> -n <namespace_name>`。  
    	`-n` 参数是指在哪个 namespace 下。当创建项目的时候，会同时创建一个同名的 namespace。指定了 namespace，就指定了项目范围。  
    	2. 提供 cluster role：`kubectl create clusterrolebinding <namespace_name>:<service_account_name>:<cluster_role_name> --clusterrole=cluster-admin --serviceaccount=<namespace_name>:<service_account_name>`。  
    参考[Kubernetes 的官方文档](https://kubernetes.io/docs/reference/access-authn-authz/rbac/)。

  * `volumes` 和 `nodeSelector` 字段：Jenkins 需要管理数据。在一个 Kubernetes 集群，可以考虑两种部署的方式：使用 PVC 或主机目录。  
    此处以使用主机目录为例，其它存储方式请参考[Kubernetes 的官方文档](https://kubernetes.io/docs/concepts/storage/dynamic-provisioning/)。  
    * 选择目标主机：在命令行中执行 `kubectl get nodes`，获取集群中所有的节点，例如：  

    ```shell
    NAME        STATUS    ROLES     AGE       VERSION
    10.0.0.10   Ready     master    103d      v1.9.1
    10.0.0.11   Ready     node      103d      v1.9.1
    ```    
    * 修改 `nodeSelector` 参数，将 Jenkins 容器部署在填写的 node 主机上。例如：`kubernetes.io/hostname: 10.0.0.10`。  
    * 修改 `volumes` 参数，`<jenkins-home>` 的 `hostPath` 路径是设置主机上 Jenkins 数据存储目标的目录；`<docker>` 的  `hostPath` 路径是设置 docker.sock 目标的目录。

* `ports` 的 `nodePort` 字段：`http` 的 `nodePort: <port>` 字段可以制定外网访问的目标端口，此端口会通过 master 节点的 IP 地址和此端口访问 Jenkins 服务。`jnlp` 的 `nodePort: <port>` 字段可以指定静态 Jenkins node 访问 Jenkins master 的端口。  
{{% notice info %}}
设置的端口需要在 `kube-apiserver` 允许的范围内。参考 master 节点中的 `/etc/kubernetes/manifests/kube-apiserver.yaml` 文件。如果让 Kubernetes 自动分配端口，需要删除 `nodePort` 字段。
{{% /notice %}} 