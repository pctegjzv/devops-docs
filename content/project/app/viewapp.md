+++
title = "查看应用详情"
description = "查看已创建的应用详情，例如：基本信息、查看日志、增减 pod 数量、更新容器等。"
weight = 2
+++

查看已创建的应用详情，例如：基本信息、查看日志、增减 pod 数量、更新容器等。

**操作步骤**

1. 用具有项目管理权限的成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，再单击 **应用**。

2. 在应用页面，单击要查看的 ***应用名称***。

3. 在应用详情页面，查看应用的 **Deployment**：

   * 基本信息：应用名称、容器信息等。

   * 单击容器的 **日志**，在 **日志** 窗口，选择不同的 pod，查看日志。日志默认是根据时间自动更新。

     * 单击 **查找**，在框中输入关键字，日志会自动匹配搜索结果，并支持前后搜索查看。

     * 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。

     * 单击 **下载**，下载日志到本地。

     * 单击 **全屏**，全屏查看日志；单击 **退出全屏**，退出全屏模式。

   * 增减 pod 数量：单击 ![add1](/img/add1.png)，增加一个 pod 数量；单击 ![minus](/img/minus.png)，减少一个 pod 数量。

   * 更新容器，参考[更新容器]({{< relref "project/app/updatecontainer.md" >}})。

4. 如果应用包含流水线，查看应用的 **流水线**：

   * 单击 ***流水线名称***，跳转到流水线详情页面。

   * 单击 ![operations](/img/operations.png)，选择 **执行** 或 **删除** 流水线。参考[执行流水线]({{< relref "project/pipeline/startpipeline.md" >}})和[删除流水线]({{< relref "project/pipeline/deletepipeline.md" >}})。

5. 查看应用的 **其他资源**，YAML 中 `kind` 的资源类型，例如 Service、Ingress。

6. 单击 **Yaml 更新**，更新应用。参考[更新应用]({{< relref "project/app/updateapp.md" >}})。
7. 单击 ![operations](/img/more.png)，再单击 **删除应用**。参考[删除应用]({{< relref "project/app/deleteapp.md" >}})。