+++
title = "通过 YAML 创建"
description = ""
weight = 1
+++

通过 YAML 编排文件来创建应用，并包含服务、Jenkins 流水线，具有便捷性、灵活性和可维护性。

**操作步骤**

1. 用具有项目管理权限的成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，在这个项目中创建应用。

2. 在应用页面，单击 **创建应用**。

3. 在 **创建应用** 窗口，单击 **YAML 创建**。

4. 在 YAML 创建页面，在 **YAML (读写)** 区域，输入创建应用的 YAML 内容。可以参考样例：[步骤 7](#jump)。

5. 同时允许以下操作：

   - 单击 **恢复**，回退到上一次的编辑状态。

   - 单击 **清空**，清空所有编辑内容。

   - 单击 **查找**，在框中输入关键字，会自动匹配搜索结果，并支持前后搜索查看。

   - 单击 **复制**，复制编辑内容。

   - 单击 **日间** 或 **夜间** 模式，屏幕会自动调节成对应的查看模式。

   - 单击 **全屏**，全屏查看；单击 **退出全屏**，退出全屏模式。

   - 单击 **显示更新**，更新的 YAML 内容会以不同颜色显示。

6. 编辑好 YAML 内容后，单击 **创建**。

7. 参考样例：<span id="jump"></span>

`````yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
    labels:
      app: yaml-createapp
    name: yaml-createapp
spec:
    replicas: 1
    template:
      metadata:
        labels:
          app: yaml-createapp
      spec:
        containers:
        - image: index.alauda.cn/alauda/helloworld
        name: yaml-createapp
        ports:
        - name: http
            containerPort: 8080
        env:
        - name: HTTP_PORT
            value: "8080"
        volumes:
        - name: yaml-createapp-data
---
apiVersion: v1
kind: Service
metadata:
    labels:
      app: yaml-createapp
    name: yaml-createapp
spec:
    type: NodePort
    ports:
    - name: http
      port: 30301
      protocol: TCP
      targetPort: 30301
    selector:
      app: yaml-createapp

---
apiVersion: devops.alauda.io/v1alpha1
kind: PipelineConfig
metadata:
    name: pipeline
    namespace: project
    labels:
      app: yaml-createapp
spec:
    jenkinsBinding:
      name: jenkins
    runPolicy: Serial
    strategy:
      jenkins:
        jenkinsfile: |
        node('agent') {
            stage 'build'
            openshiftBuild(buildConfig: 'ruby-sample-build'， showBuildLogs: 'true')
            stage 'deploy'
            openshiftDeploy(deploymentConfig: 'frontend')
        }
    hooks:
    - type: httpRequest
      events:
      - PipelineConfigCreated
      - PipelineConfigUpdated
      - PipelineConfigDeleted
      - PipelineStarted
      - PipelineStopped
      httpRequest:
        uri: "http://bitbucket.org/hook"
        method: POST
        headers:
          Authorization: "Token 123456"
    source:
      git:
        uri: "https://github.com/alauda/alauda"
        ref: "master"
`````

