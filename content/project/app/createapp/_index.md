+++
title = "创建应用"
description = "在项目中创建应用，并配置 Jenkins 流水线，便于服务的零宕机升级。"
weight = 1
+++

在项目中创建应用，并配置 Jenkins 流水线，便于服务的零宕机升级。

**操作步骤**

1. 用具有项目管理权限的成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，在这个项目中创建应用。

2. 在应用页面，单击 **创建应用**。

3. 在 **创建应用** 窗口，支持三种方式创建应用：

   * **YAML 创建**：通过 YAML 编排文件来创建应用，具有便捷性、灵活性和可维护性。参考[通过 YAML 创建]({{< relref "project/app/createapp/yaml.md" >}})。

   * **模板创建**：通过提供的 **Java**、**Golang**、**Python** 或 **PHP** 语言模板，设置镜像、网络、流水线等来创建应用。

     * [通过 Java 模板创建]({{< relref "project/app/createapp/java.md" >}})

     * [通过 Golang 模板创建]({{< relref "project/app/createapp/golang.md" >}})

     * [通过 Python 模板创建]({{< relref "project/app/createapp/python.md" >}})

     * [通过 PHP 模板创建]({{< relref "project/app/createapp/php.md" >}})

   * **镜像创建**：通过镜像，关联 secret，设置网络和环境变量等来创建应用。镜像创建的应用不包含 Jenkins 流水线。参考[通过镜像创建]({{< relref "project/app/createapp/image.md" >}})。