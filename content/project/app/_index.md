+++
title = "应用"
description = ""
weight = 1
+++

服务是一组 pods 的逻辑集合，以及设定的访问策略，可以看作是一个微服务。通过服务，应用可以方便地实现服务发现和负载均衡，并实现零宕机升级。每个应用可包含一个或多个相互关联的服务，实现快速的业务部署和跨云迁移。

{{%children style="card" description="true" %}}





