+++
title = "取消执行流水线"
description = "取消正在执行或排队中的 Jenkins 流水线。"
weight = 5
+++

取消正在执行或排队中的 Jenkins 流水线。

**操作步骤**

1. 项目成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，再单击 **流水线**。

2. 在流水线页面，单击要取消执行的 ***流水线名称***。

3. 在流水线详情页面的 **详情信息** 栏，在 **执行记录** 区域，找到要取消执行的 ID。

4. 单击 ![operations](/img/operations.png)，再单击 **取消执行**。

5. 在新窗口，单击 **取消执行**。  
   执行状态变成 **已取消**。
    