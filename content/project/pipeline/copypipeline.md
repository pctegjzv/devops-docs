+++
title = "复制流水线"
description = "复制已创建的流水线配置，并通过创建流水线流程，生成一条新的流水线。"
weight = 10
+++

复制已创建的流水线配置，并通过创建流水线流程，生成一条新的流水线。

**操作步骤**

1. 项目成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，再单击 **流水线**。

2. 在流水线页面，可以通过以下两种方式复制流水线：

   * 找到要复制的流水线名称，单击 ![operations](/img/operations.png)，再单击 **复制**。

   * 单击要复制的 ***流水线名称***，在流水线详情页面，单击 **操作** > **复制**。

3. 使用复制的流水线配置作为默认配置，重新配置后，生成新的流水线，与创建流水线的操作相同。
	
	* 如果复制的流水线是使用代码仓库的 Jenkinsfile 创建的，参考[使用代码仓库的 Jenkinsfile 创建流水线]({{< relref "project/pipeline/createfilepipeline/createcodefilepipeline.md" >}})。
	
	* 如果复制的流水线时使用页面编写的 Jenkinsfile 创建的，参考[使用页面编写的 Jenkinsfile 创建流水线]({{< relref "project/pipeline/createfilepipeline/createeditfilepipeline.md" >}})。 

