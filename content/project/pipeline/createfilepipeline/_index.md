+++
title = "使用 Jenkinsfile 创建流水线"
description = "使用自定义的 Jenkinsfile 创建流水线"
weight = 1
+++

使用自定义的 Jenkinsfile 创建流水线。主要包括以下三个环节:

1. **基本信息**：配置流水线的基本信息。

2. **Jenkinsfile**: 配置构建流水线使用的 Jenkinsfile。支持以下两种配置 Jenkinsfile 的方式。
	
	* 选择代码仓库中托管的 Jenkinsfile，参考[使用代码仓库的 Jenkinsfile 创建流水线]({{< relref "project/pipeline/createfilepipeline/createcodefilepipeline.md" >}})。
	
	* 直接在页面上编写 Jenkinsfile，参考[使用页面编写的 Jenkinsfile 创建流水线]({{< relref "project/pipeline/createfilepipeline/createeditfilepipeline.md" >}})。

3. **触发器**：配置触发流水线的方式。






