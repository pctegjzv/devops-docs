+++
title = "使用代码仓库的 Jenkinsfile 创建流水线"
Description = ""
weight = 1
+++

使用代码仓库中托管的 Jenkinsfile 创建流水线。

**操作步骤**

1. 项目成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，再单击 **流水线** > **创建流水线**。

2. ​单击 **脚本创建**。

3. 在 **基本信息** 环节，配置流水线的基本信息，参数说明如下：

	* 在 **名称** 框中，输入 Jenkins 流水线的名称。  
	名称支持小写英文字母、数字 0 ~ 9、中横线。字符数大于等于 1 个，小于等于 36 个。不支持以中横线结尾。
	
	* （非必填）在 **显示名称** 框中，输入 Jenkins 流水线的显示名称，支持中文字符。如未输入，默认显示为空。

	* 在 **Jenkins 实例** 框中，选择一个已创建的 Jenkins 实例。
	
	* （非必填）在 **所属应用** 框中，选择流水线所属的应用。
	
	* 在 **Jenkinsfile 位置** 框中，选择获取 Jenkinsfile 文件的方式，使用代码仓库中托管的 Jenkinsfile 时选择 **代码仓库**。

	配置完成后单击 **下一步**。 

4. 在 **Jenkinsfile** 环节，选择代码仓库托管的 Jenkinsfile。

	* **代码仓库**：配置代码仓库和访问凭据。
		
		1. 单击 **选择**。
		
		2. 在 **代码仓库** 窗口，选择一个已有的代码仓库；或者手动输入代码仓库地址，例如：```https://github.com/example/example.git```。
		3. 选择一个已创建的 Secret，用于匹配发送请求的用户身份。单击 ***添加 Secret*** 可创建一个新的 Secret，在新窗口中配置 Secret 参数，参考[创建 Secret]({{< relref "project/secret/createsecret.md" >}})。  
	
		4. 单击 **确认** 完成操作。
	
	* **代码分支**：输入代码所在的分支，例如```master```。
	
	* **脚本路径**：输入代码仓库中的 Jenkinsfile 文本文件的路径，例如```Jenkinsfile```，表示位于根目录的 Jenkinsfile。  
	
	配置完成后单击 **下一步**。
	
5. 在 **触发器** 环节，根据实际策略，开启或关闭代码触发器或定时触发器。开启触发器并配置相应的触发器规则，可实现流水线的自动触发。

	* **代码触发器**：在指定的时间检查代码仓库是否有变更，如有变更，将触发流水线。支持选择预设的触发器规则，或手动输入自定义触发器规则。  
	例如选择 **每 2 分钟检查新提交**，则每隔 2 分钟系统会自动检查代码仓库是否有新提交的变更，若有新提交的变更则会执行流水线。

	* **定时触发器**：在指定的时间触发流水线，选择 **定时触发器** 后，在 **触发器规则** 栏中，输入自定义触发器规则。

	自定义触发器规则的语法说明请参考[触发器规则]({{< relref "project/pipeline/timetrigger.md" >}})。
	
	配置完成后，单击 **创建**。流水线创建完成后，跳转到流水线详情页面，查看流水线详情。
