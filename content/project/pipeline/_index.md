+++
title = "流水线"
description = ""
weight = 2
+++

流水线可实现项目从构建、测试到发布的完整流程，项目成员在使用 YAML 或应用模板创建应用时，可以同时创建 Jenkins 流水线。 
此外，支持使用流水线模板或 Jenkinsfile 创建流水线，并提供查看流水线执行记录、更新流水线、复制流水线等功能。

{{%children style="card" description="true" %}}





