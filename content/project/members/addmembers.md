+++
title = "添加成员"
description = "在项目中添加成员，管理和运维项目资源。成员角色包括开发测试和项目经理。"
weight = 1
+++

在项目中添加成员，管理和运维项目资源。成员角色包括开发测试和项目经理。

**操作步骤**

1. 用具有项目管理权限的成员登录 DevOps 平台，在项目列表页面，单击一个 ***项目名称***，再单击 **成员**。

2. 在项目成员页面，在 **项目成员** 区域的框中，输入成员的邮箱地址，并选择 **开发测试** 或 **项目经理** 角色中的一个。  
添加项目经理后，在项目页面，可以看到项目经理的邮箱地址。

3. 单击 **添加成员**。  
   添加成员后，在列表中可以看到已添加的成员信息。