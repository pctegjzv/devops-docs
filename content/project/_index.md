+++
title = "项目成员"
description = ""
weight = 3
alwaysopen = true
+++

项目成员专注于项目的具体业务，开发测试负责项目中具体业务的开发和测试，项目经理负责对项目的监控和管理。

- [应用]({{< relref "project/app/_index.md" >}})

- [流水线]({{< relref "project/pipeline/_index.md" >}})

- [Secret]({{< relref "project/secret/_index.md" >}})

- [成员]({{< relref "project/members/_index.md" >}})


