# DevOps平台文档网站

[TOC]

## Getting started

### Installing the tools

#### Automagically

run: `make setup`

#### Manual install

  1. Install [brew](https://brew.sh/): 
      `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
  2. Install [hugo](https://gohugo.io):
     `brew install hugo`
  3. Install [sass](https://sass-lang.com/):
     `sudo gem install sass`
  4. Install [go](https://golang.org):
     `brew install go`
  5. Install [linkcrawler](https://github.com/danielfbm/linkcrawler)
     `go get -u github.com/danielfbm/linkcrawler`
  6. Install [wkhtmltopdf](https://wkhtmltopdf.org/)
     `curl -L -O https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.4/wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg && open wkhtmltox-0.12.4_osx-cocoa-x86-64.pkg`



### Developing

In the root folder of the project: `make run -j2` or `hugo server`

If there is a need to change the flags used to start hugo please check the [documentation](https://gohugo.io/getting-started/usage/) or run `hugo help`

Once the hugo server starts it will reload automatically once the content has changed

### Building website's PDF

Quit your hugo server if any and type:

`make build-pdf`

This will create a pdf file inside the `static/docs.pdf`

### Adding content

hugo comes with many commands to add new documents quickly, but it is strongly recommended to understand the underlying structure and the why things work to avoid unexpected results. For more information regarding content management please check the [documentation](https://gohugo.io/content-management/)



