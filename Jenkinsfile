// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// global variables for pipeline
def GIT_BRANCH
def GIT_COMMIT
def deployment
// image can be used for promoting...
def IMAGE
def IMAGE_E2E
def CURRENT_VERSION
def RELEASE_VERSION
def RELEASE_BUILD
def code_data
def DEBUG = false
pipeline {
    // 运行node条件
    // 为了扩容jenkins的功能一般情况会分开一些功能到不同的node上面
    // 这样每个node作用比较清晰，并可以并行处理更多的任务量
    agent {
        label 'all'
    }

    // (optional) 流水线全局设置
    options {
        // 保留多少流水线记录（建议不放在jenkinsfile里面）
        buildDiscarder(logRotator(numToKeepStr: '10'))

        // 不允许并行执行
        disableConcurrentBuilds()
    }
    //(optional) 环境变量
    environment {
        // def root = tool name: 'go1.9', type: 'go'
        // GOROOT = "${root}"
        // GOPATH = '/go'
        // PATH = "${root}/bin:${GOPATH}/bin:$PATH"
        FOLDER = "."

        // for building an scanning
        REPOSITORY = "devops-docs"
        OWNER = "mathildetech"
        // sonar feedback user
        // needs to change together with the credentialsID
        BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
        SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
        NAMESPACE = "alauda-system"
        DEPLOYMENT = "devops-docs"
        CONTAINER = "backend"
        DINGDING_BOT = "devops-chat-bot"
        TAG_CREDENTIALS = "alaudabot-bitbucket"
        E2E_IMAGE = "index.alauda.cn/alaudak8s/diablo-e2e"
    }
    // stages
    stages {
        stage('Checkout') {
            steps {
                script {
                    // checkout code
                    def scmVars = checkout scm
                    // extract git information
                    env.GIT_COMMIT = scmVars.GIT_COMMIT
                    env.GIT_BRANCH = scmVars.GIT_BRANCH
                    GIT_COMMIT = "${scmVars.GIT_COMMIT}"
                    GIT_BRANCH = "${scmVars.GIT_BRANCH}"
                    RELEASE_BUILD = "${env.BUILD_NUMBER}"
                    RELEASE_VERSION = readFile('.version').trim()
                    RELEASE_BUILD = "${RELEASE_VERSION}.${env.BUILD_NUMBER}"
                    if (GIT_BRANCH != "master") {
                        def branch = GIT_BRANCH.replace("/","-").replace("_", "-")
                        RELEASE_BUILD = "${RELEASE_VERSION}.${branch}.${env.BUILD_NUMBER}"
                }
            }
            // installing golang coverage and report tools
            sh """
                go get -u github.com/alauda/gitversion
                go get github.com/magefile/mage
                go get -d github.com/gohugoio/hugo
                cd $GOPATH/src/github.com/gohugoio/hugo
                git checkout v0.40.2
                mage vendor
                mage install
            """
            script {
                if (GIT_BRANCH == "master") {
                    sh "gitversion patch ${RELEASE_VERSION} > patch"
                    RELEASE_BUILD = readFile("patch").trim()
                }
                echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
            }
        }
    }

      stage('CI'){
        steps {
            script {
                // build
                sh """
                    make build-site
                """
                // currently is building code inside the container
                IMAGE = deploy.dockerBuild(
                    "artifacts/Dockerfile", //Dockerfile
                    ".", // build context
                    "index.alauda.cn/alaudak8s/devops-docs", // repo address
                    "${RELEASE_BUILD}", // tag
                    "alaudak8s", // credentials for pushing
                )
                // start and push
                IMAGE.start().push().push("${GIT_COMMIT}")
            }
        }
      }
      // after build it should start deploying
      stage('Deploying') {
          steps {
              echo "here we can start to deploy"
              script {
                  // setup kubectl
                  if (GIT_BRANCH == "master") {
                      // master is already merged
                      deploy.setupStaging()

                  } else {
                      // pull-requests
                      deploy.setupInt()
                  }
                  // saving current state
                  CURRENT_VERSION = deploy.getDeployment(NAMESPACE, DEPLOYMENT)
                  // starts deploying
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
              }
          }
      }
      stage('Promoting') {
          // limit this stage to master only
          when {
              expression {
                  GIT_BRANCH == "master"
              }
          }
          steps {
              script {
                  // setup kubectl
                  deploy.setupProd()

                  // promote to release
                  IMAGE.push("release")

                  // update production environment
                  deploy.updateDeployment(
                      NAMESPACE,
                      DEPLOYMENT,
                      // just built image
                      IMAGE.getImage(),
                      CONTAINER,
                  )
                  // adding tag to the current commit
                  withCredentials([usernamePassword(credentialsId: TAG_CREDENTIALS, passwordVariable: 'GIT_PASSWORD', usernameVariable: 'GIT_USERNAME')]) {
                    sh "git tag -l | xargs git tag -d" // clean local tags
                    sh """
                        git config --global user.email "alaudabot@alauda.io"
                        git config --global user.name "Alauda Bot"
                    """
                    def repo = "https://${GIT_USERNAME}:${GIT_PASSWORD}@bitbucket.org/${OWNER}/${REPOSITORY}.git"
                    sh "git fetch --tags ${repo}" // retrieve all tags
                    sh("git tag -a ${RELEASE_BUILD} -m 'auto add release tag by jenkins'")
                    sh("git push ${repo} --tags")
                }
                build job: '../../charts-pipeline', parameters: [
                  [$class: 'StringParameterValue', name: 'CHART', value: 'devops'],
                  [$class: 'StringParameterValue', name: 'VERSION', value: ''],
                  [$class: 'StringParameterValue', name: 'COMPONENT', value: 'docs'],
                  [$class: 'StringParameterValue', name: 'IMAGE_TAG', value: RELEASE_BUILD],
                  [$class: 'BooleanParameterValue', name: 'DEBUG', value: false],
                  [$class: 'StringParameterValue', name: 'ENV', value: ''],
                ], wait: false
              }
          }
      }

    }

    // (optional)
    // happens at the end of the pipeline
    post {
        // 成功
        success {
          echo "Horay!"
          script {
            if (GIT_BRANCH == "master") {
                deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "上线啦！", RELEASE_BUILD)
            } else {
                deploy.notificationSuccess(DEPLOYMENT, DINGDING_BOT, "流水线完成了", RELEASE_BUILD)
            }
          }
        }

        // 失败
        failure {
            echo "damn!"
            // check the npm log
            // fails lets check if it
            script {
                if (CURRENT_VERSION != null) {
                  if (GIT_BRANCH == "master") {
                    deploy.setupStaging()
                  } else {
                    deploy.setupInt()
                  }
                    deploy.rollbackDeployment(
                        NAMESPACE,
                        DEPLOYMENT,
                        CONTAINER,
                        CURRENT_VERSION,
                    )
                }
                deploy.notificationFailed(DEPLOYMENT, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
            }
        }
    }
}
